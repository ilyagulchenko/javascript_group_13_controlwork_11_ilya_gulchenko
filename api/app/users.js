const express = require('express');
const User = require("../models/User");
const mongoose = require("mongoose");
const auth = require("../middleware/auth");
const bcrypt = require("bcrypt");

const router = express.Router();

router.post('/', async (req,res,next) => {
    try {
        const user = new User(req.body);
        user.generateTokens();
        await user.save();

        return res.send(user);
    } catch (error) {
        if (error instanceof mongoose.Error.ValidationError) {
            return res.status(400).send(error);
        }

        return next(error);
    }
});

router.get('/', async (req,res,next) => {
    try {
        const users = await User.find();
        return res.send(users);
    } catch (error) {
        next(error);
    }
});

router.post('/sessions', async (req, res, next) => {
    try {
        const user = await User.findOne({username: req.body.username});

        if (!user) {
            return res.status(400).send({error: 'Username not found'});
        }

        const isMatch = await bcrypt.compare(req.body.password, user.password);

        if (!isMatch) {
            return res.status(400).send({error: 'Password is wrong'})
        }

        user.generateTokens();
        await user.save()

        return res.send(user);
    } catch (error) {
        next(error);
    }
});

router.get('/secret', auth, async (req,res,next) => {
    try {
        return res.send({message: 'Hello, ' + req.user.username});
    } catch (e) {
        next(e);
    }
});

module.exports = router;
