const express = require('express');
const multer = require('multer');
const path = require('path');
const { nanoid } = require('nanoid');
const config = require('../config');
const Product = require("../models/Product");

const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

router.get('/', async (req,res,next) => {
    try {
        const query = {};

        const fields = [
            {
                path:'user',
                select:'name phone'
            },
            {
                path:'category',
                select:'title'
            }
        ];

        if (req.query.filter === 'image') {
            query.image = {$exists: true};
        }

        const products = await Product.find(query).populate(fields);

        return res.send(products);
    } catch (e) {
        next(e);
    }
});

router.get('/:id',async (req,res, next) => {
    try {
        const product = await Product.findById(req.params.id);

        if (!product) {
            return res.status(404).send({message: 'Not found!'});
        }

        return res.send(product);
    } catch (e) {
        next(e);
    }
});

router.post('/', upload.single('image'), async (req, res, next) => {
    try {
        if (!req.body.title || !req.body.description || !req.body.category) {
            return res.status(400).send({message: 'Title and description and category are required!'});
        }

        const productData = {
            category: req.body.category,
            user: req.body.user,
            title: req.body.title,
            description: req.body.description,
            price: parseFloat(req.body.price),
            image: null,
        };

        if (req.file) {
            productData.image = req.file.filename;
        }

        const product = new Product(productData);

        await product.save();

        return res.send(productData);
    } catch (e) {
        next(e);
    }
});

module.exports = router;
