const mongoose = require('mongoose');
const config = require('./config');
const Category = require("./models/Category");
const Product = require("./models/Product");
const User = require("./models/User");

const run = async () => {
    await mongoose.connect(config.mongoConfig.db, config.mongoConfig.options);

    const collections = await mongoose.connection.db.listCollections().toArray();

    for (const coll of collections) {
        await mongoose.connection.db.dropCollection(coll.name);
    }

    const [user1, user2, user3] = await User.create({
        username: 'Jon_Devil',
        password: '123',
        name: 'Jon Swift',
        phone: '+789 338 25 96',
        token: 'asdfghjkl'
    }, {
        username: 'Levin-stone',
        password: 'qwert',
        name: 'Sam Livingstone',
        phone: '+996 500 55 96 55',
        token: 'qwertyuiop'
    }, {
        username: 'RedQuill',
        password: 'asdf',
        name: 'Ridley Scott',
        phone: '+777 77 77 77',
        token: 'zxcvbnm'
    })

    const [clothes, tech, food] = await Category.create({
        title: 'Clothes',
        description: 'Clothes are items worn on the body.'
    }, {
        title: 'Technique',
        description: 'Technique - generalizing name of complex devices, mechanisms, systems.'
    }, {
        title: 'Food',
        description: 'Food is any substance consumed to provide nutritional support for an organism.'
    });

    await Product.create({
        category: clothes,
        user: user1,
        title: 'Nike Phantom GT2 Dynamic Fit Elite FG',
        description: 'Футбольные бутсы для игры на твердом грунте',
        price: 24799,
        image: 'cloth.jpeg'
    }, {
        category: tech,
        user: user2,
        title: 'Монитор 27" Samsung LF27T350FHIXCI',
        description: 'тип матрицы: IPS, 75 Гц разрешение: 1920x1080 (16:9) время отклика: 5 мс',
        price: 23480,
        image: 'monitor.jpg'
    }, {
        category: food,
        user: user3,
        title: 'Жгучая пепперони',
        description: 'Сыр моцарелла, пицца-соус, пепперони, халапеньо, лук красный',
        price: 365,
        image: 'pizza.jpg'
    });

    await mongoose.connection.close();
};

run().catch(e => console.error(e));
