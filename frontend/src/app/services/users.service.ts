import {HttpClient} from '@angular/common/http';
import {LoginUserData, RegisterUserData, User} from '../models/user.model';
import {Injectable} from '@angular/core';
import {environment as env} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http: HttpClient) {}

  registerUser(userdata: RegisterUserData) {
    return this.http.post<User>(env.apiUrl + '/users', userdata);
  }

  login(userData: LoginUserData) {
    return this.http.post<User>(env.apiUrl + '/users/sessions', userData);
  }
}
