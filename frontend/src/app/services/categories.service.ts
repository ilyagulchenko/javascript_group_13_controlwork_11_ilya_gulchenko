import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment as env } from '../../environments/environment';
import { map } from 'rxjs/operators';
import { ApiCategoryData, Category } from '../models/category.model';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {

  constructor(private http: HttpClient) {}

  getCategories() {
    return this.http.get<ApiCategoryData[]>(env.apiUrl + '/categories').pipe(
      map(response => {
        return response.map(categoryData => {
          return new Category(
            categoryData._id,
            categoryData.title,
            categoryData.description
          );
        });
      })
    )
  }
}
