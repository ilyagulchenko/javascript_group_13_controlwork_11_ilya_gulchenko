import { HttpClient } from '@angular/common/http';
import {ApiProductData, Product, ProductData} from '../models/product.model';
import { environment as env } from '../../environments/environment';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(private http: HttpClient) {}

  getProducts() {
    return this.http.get<ApiProductData[]>(env.apiUrl + '/products').pipe(
      map(response => {
        return response.map(productData => {
          return new Product(
            productData._id,
            productData.category,
            productData.user,
            productData.title,
            productData.description,
            productData.price,
            productData.image
          );
        });
      })
    )
  }

  getProduct(id: any) {
    return this.http.get<ApiProductData>(env.apiUrl + `/products/${id}`).pipe(
      map(response => {
          return new Product(
            response._id,
            response.category,
            response.user,
            response.title,
            response.description,
            response.price,
            response.image
          );
      })
    )
  }

  addNewProduct(productData: ProductData) {
    const formData = new FormData();

    Object.keys(productData).forEach(key => {
      if (productData[key] !== null) {
        formData.append(key, productData[key]);
      }
    });

    return this.http.post(env.apiUrl + '/products', formData);
  }
}
