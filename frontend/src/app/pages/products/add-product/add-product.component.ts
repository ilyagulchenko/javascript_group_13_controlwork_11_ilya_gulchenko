import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from '../../../store/types';
import { ProductData } from '../../../models/product.model';
import { createProductRequest } from '../../../store/products.actions';
import { Category } from '../../../models/category.model';
import { fetchCategoriesRequest } from '../../../store/categories.actions';
import { User } from '../../../models/user.model';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.sass']
})
export class AddProductComponent implements OnInit {
  @ViewChild('f') form!: NgForm;
  categories: Observable<Category[]>
  loading: Observable<boolean>;
  error: Observable<string | null>;
  user: Observable<null | User>;

  constructor(private store: Store<AppState>) {
    this.categories = store.select(state => state.categories.categories)
    this.loading = store.select(state => state.products.createLoading);
    this.error = store.select(state => state.products.createError);
    this.user = store.select(state => state.users.user);
  }

  ngOnInit(): void {
    this.store.dispatch(fetchCategoriesRequest());
  }

  onSubmit() {
    const productData: ProductData = this.form.value;
    this.store.dispatch(createProductRequest({productData}));
  }

}
