import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs';
import {Product} from '../../../models/product.model';
import {AppState} from '../../../store/types';
import {ActivatedRoute} from '@angular/router';
import {Store} from '@ngrx/store';
import {getProductRequest} from '../../../store/products.actions';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.sass']
})
export class ProductDetailsComponent implements OnInit {
  product: Observable<Product>;
  loading: Observable<boolean>;
  error: Observable<string | null>;

  constructor(private store: Store<AppState>, private route: ActivatedRoute) {
    this.product = store.select(state => state.product.product);
    this.loading = store.select(state => state.product.getLoading);
    this.error = store.select(state => state.product.getError);
  }

  ngOnInit(): void {
    // @ts-ignore
    const id: string = this.route.params.value.id
    this.store.dispatch(getProductRequest({id}));
  }

}
