import { ProductsState } from './types';
import {createReducer, on} from '@ngrx/store';
import {
  createProductFailure,
  createProductRequest, createProductSuccess,
  fetchProductsFailure,
  fetchProductsRequest,
  fetchProductsSuccess, getProductFailure, getProductRequest, getProductSuccess
} from './products.actions';

const initialState: ProductsState = {
  products: [],
  fetchLoading: false,
  fetchError: null,
  createLoading: false,
  createError: null,
}

export const productsReducer = createReducer(
  initialState,
  on(fetchProductsRequest, state => ({...state, fetchLoading: true})),
  on(fetchProductsSuccess, (state, {products}) => ({...state, fetchLoading: false, products})),
  on(fetchProductsFailure, (state, {error}) => ({...state, fetchLoading: false, fetchError: error})),

  on(createProductRequest, state => ({...state, createLoading: true})),
  on(createProductSuccess, state => ({...state, createLoading: false})),
  on(createProductFailure, (state, {error}) => ({...state, createLoading: false, createError: error})),

  on(getProductRequest, state => ({...state, getLoading: true})),
  on(getProductSuccess, (state, {product}) => ({...state, getLoading: false, product})),
  on(getProductFailure, (state, {error}) => ({...state, getLoading: false, getError: error})),
);
