import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { ProductsService } from '../services/products.service';
import {
  createProductFailure,
  createProductRequest, createProductSuccess,
  fetchProductsFailure,
  fetchProductsRequest,
  fetchProductsSuccess, getProductFailure, getProductRequest, getProductSuccess
} from './products.actions';
import { catchError, mergeMap, of, tap } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable()
export class ProductsEffects {

  constructor(
    private actions: Actions,
    private productsService: ProductsService,
    private router: Router
  ) {}

  fetchProducts = createEffect(() => this.actions.pipe(
    ofType(fetchProductsRequest),
    mergeMap(() => this.productsService.getProducts().pipe(
      map( products => fetchProductsSuccess({products})),
      catchError(() => of(fetchProductsFailure({error: 'Something wrong'})))
    ))
  ));

  createProduct = createEffect(() => this.actions.pipe(
    ofType(createProductRequest),
    mergeMap(({productData}) => this.productsService.addNewProduct(productData).pipe(
      map(() => createProductSuccess()),
      tap(() => this.router.navigate(['/'])),
      catchError(() => of(createProductFailure({error: 'Wrong data'})))
    ))
  ));

  getProduct = createEffect(() => this.actions.pipe(
    ofType(getProductRequest),
    mergeMap(({id}) => this.productsService.getProduct(id).pipe(
      map(product => getProductSuccess({product})),
      catchError(() => of(getProductFailure({error: 'Something wrong!'})))
    ))
  ));
}
