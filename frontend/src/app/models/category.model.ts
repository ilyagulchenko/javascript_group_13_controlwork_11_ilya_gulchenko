export class Category {
  constructor(
    public id: string,
    public title: string,
    public description: string,
  ) {}
}

export interface ApiCategoryData {
  _id: string,
  title: string,
  description: string
}
