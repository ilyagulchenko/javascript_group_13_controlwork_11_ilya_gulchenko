export class Product {
  constructor(
    public id: string,
    public category: {
      title: string
    },
    public user: {
      _id: string,
      name: string,
      phone: string
    },
    public title: string,
    public description: string,
    public price: number,
    public image: string,
  ) {}
}

export interface ApiProductData {
  _id: string,
  category: {
    title: string
  },
  user: {
    _id: string,
    name: string,
    phone: string
  },
  title: string,
  price: number,
  description: string,
  image: string
}

export interface ProductData {
  [key: string]: any;
  title: string;
  description: string;
  price: number;
  user: string;
  category: string;
  image: File;
}
