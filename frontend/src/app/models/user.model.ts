export interface User {
  _id: string,
  username: string,
  name: string,
  phone: string,
  token: string,
}

export interface RegisterUserData {
  username: string,
  password: string
}

export interface LoginUserData {
  username: string,
  password: string
}

export interface RegisterError {
  errors: {
    password: FieldError,
    username: FieldError
  }
}

export interface LoginError {
  error: string
}

export interface FieldError {
  message: string
}
