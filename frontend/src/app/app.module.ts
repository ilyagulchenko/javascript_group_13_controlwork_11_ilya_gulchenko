import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {ActionReducer, MetaReducer, StoreModule} from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutComponent } from './ui/layout/layout.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { ProductsComponent } from './pages/products/products.component';
import { AddProductComponent } from './pages/products/add-product/add-product.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatCardModule} from '@angular/material/card';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { ImagePipe } from './pipes/image.pipe';
import {productsReducer} from './store/products.reducer';
import {ProductsEffects} from './store/products.effects';
import {HttpClientModule} from '@angular/common/http';
import {FileInputComponent} from './ui/file-input/file-input.component';
import {FormsModule} from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {categoriesReducer} from './store/categories.reducer';
import {CategoriesEffects} from './store/categories.effects';
import { CenteredCardComponent } from './ui/centered-card/centered-card.component';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import {MatMenuModule} from '@angular/material/menu';
import {usersReducer} from './store/users.reducer';
import {UsersEffects} from './store/users.effects';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { localStorageSync } from 'ngrx-store-localstorage';
import { ProductDetailsComponent } from './pages/products/product-details/product-details.component';

const localStorageSyncReducer = (reducer: ActionReducer<any>) => {
  return localStorageSync({
    keys: [{users: ['user']}],
    rehydrate: true
  })(reducer);
}

const metaReducers: MetaReducer[] = [localStorageSyncReducer];

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    ProductsComponent,
    AddProductComponent,
    ImagePipe,
    FileInputComponent,
    CenteredCardComponent,
    LoginComponent,
    RegisterComponent,
    ProductDetailsComponent
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        StoreModule.forRoot({
          products: productsReducer,
          categories: categoriesReducer,
          users: usersReducer
        }, {metaReducers}),
        EffectsModule.forRoot([ProductsEffects, CategoriesEffects, UsersEffects]),
        BrowserAnimationsModule,
        LayoutModule,
        MatToolbarModule,
        MatButtonModule,
        MatSidenavModule,
        MatIconModule,
        MatListModule,
        HttpClientModule,
        FlexLayoutModule,
        MatCardModule,
        MatProgressSpinnerModule,
        FormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatMenuModule,
      MatSnackBarModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
